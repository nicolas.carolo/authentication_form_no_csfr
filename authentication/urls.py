from django.conf.urls import url
from authentication import views

urlpatterns = [
    url(r'^$', views.authentication, name='authentication')
]